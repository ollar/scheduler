from aiohttp.web import HTTPMethodNotAllowed, StreamResponse
import asyncio
import io
import re


class SSEResponse(StreamResponse):
    DEFAULT_SEPARATOR = '\r\n'
    LINE_SEP_EXPR = re.compile(r'\r\n|\r|\n')


    def __init__(self, *, status = 200, reason = None, headers = None, sep = None ):
        super().__init__(status = status, reason = reason, headers = headers)

        self.headers['Content-Type'] = 'text/event-stream'
        self.headers['Cache-Control'] = 'no-cache'
        self.headers['Connection'] = 'keep-alive'
        self.headers['X-Accel-Buffering'] = 'no'
        self.headers['Access-Control-Allow-Origin'] = '*'

        self._sep = sep if sep is not None else self.DEFAULT_SEPARATOR


    async def prepare(self, request):
        """Prepare for streaming and send HTTP headers.
        :param request: regular aiohttp.web.Request.
        """
        if request.method != 'GET':
            raise HTTPMethodNotAllowed(request.method, ['GET'])

        if not self.prepared:
            writer = await super().prepare(request)
            # explicitly enabling chunked encoding, since content length
            # usually not known beforehand.
            self.enable_chunked_encoding()
            return writer
        else:
            # hackish way to check if connection alive
            # should be updated once we have proper API in aiohttp
            # https://github.com/aio-libs/aiohttp/issues/3105
            if request.protocol.transport is None:
                # request disconnected
                raise asyncio.CancelledError()


    async def send(self, data, id=None, event=None, retry=None):
        """Send data using EventSource protocol
        :param str data: The data field for the message.
        :param str id: The event ID to set the EventSource object's last
            event ID value to.
        :param str event: The event's type. If this is specified, an event will
            be dispatched on the browser to the listener for the specified
            event name; the web site would use addEventListener() to listen
            for named events. The default event type is "message".
        :param int retry: The reconnection time to use when attempting to send
            the event. [What code handles this?] This must be an integer,
            specifying the reconnection time in milliseconds. If a non-integer
            value is specified, the field is ignored.
        """
        buffer = io.StringIO()
        if id is not None:
            buffer.write(self.LINE_SEP_EXPR.sub('', f'id: {id}'))
            buffer.write(self._sep)

        if event is not None:
            buffer.write(self.LINE_SEP_EXPR.sub('', f'event: {event}'))
            buffer.write(self._sep)

        for chunk in self.LINE_SEP_EXPR.split(data):
            buffer.write(f'data: {chunk}')
            buffer.write(self._sep)

        if retry is not None:
            if not isinstance(retry, int):
                raise TypeError('retry argument must be int')
            buffer.write(f'retry: {retry}')
            buffer.write(self._sep)

        buffer.write(self._sep)
        await self.write(buffer.getvalue().encode('utf-8'))
