from aiohttp import web
import asyncio
import logging
from .SSEResponse import SSEResponse
from utils import json_response
from .messages import messenger

routes = web.RouteTableDef()


@routes.get('/subscribe')
async def stream_handler(request):
    auth_data = request['auth_data']
    user_id = auth_data['user_id']
    response = SSEResponse()

    request.app['streams'].add(response)

    messenger.register_user(user_id)
    id = 0

    try:
        while True:
            messages = messenger.get_messages(user_id)

            await response.prepare(request)

            if messages:
                if response.prepared:
                    for message in messages:
                        _, _, data, event, retry = message
                        await response.send(data=data, event=event, retry=retry, id=id)
                        logging.info(message)
                        id += 1

                messenger.clear_messages(user_id)

            await asyncio.sleep(1)
    finally:
        request.app['streams'].discard(response)

    return response


def getRoutes():
    return routes
