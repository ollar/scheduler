import sqlite3


class Messenger():
    @staticmethod
    def create_connection():
        # con = sqlite3.connect('file::messages:?mode=memory&cache=shared', uri=True, check_same_thread=False)
        con = sqlite3.connect('messages.db?mode=memory&cache=shared', uri=True, check_same_thread=False)
        cur = con.cursor()
        return con, cur


    def __init__(self):
        con, cur = self.create_connection()

        cur.execute("""
                CREATE TABLE IF NOT EXISTS messages (
                    id INTEGER PRIMARY KEY,
                    user_id INTEGER NOT NULL,
                    data TEXT NOT NULL,
                    event TEXT,
                    retry INTEGER)
                """)
        con.close()


    def register_user(self, user_id):
        pass


    def get_messages(self, user_id, default_value=None):
        con, cur = self.create_connection()

        cur.execute("SELECT * FROM messages WHERE user_id=?", (user_id,))

        entries = cur.fetchall()

        con.close()

        return entries


    def add_message(self, user_id, message):
        if not message['data']:
            raise ValueError('Message data is mandatory')
        if not message['event']:
            raise ValueError('Message event is mandatory')

        con, cur = self.create_connection()

        cur.execute(
            """INSERT into messages (user_id, data, event, retry)
                VALUES (?,?,?,?)""",
                (user_id,message['data'],message['event'],message.get('retry'),)
            )

        con.commit()
        con.close()


    def clear_messages(self, user_id):
        con, cur = self.create_connection()

        cur.execute(
            """DELETE FROM messages
                WHERE user_id=?;""",
                (user_id,)
            )

        con.commit()
        con.close()


messenger = Messenger()