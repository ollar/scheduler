from aiohttp import web
import weakref
import logging
from .routes import getRoutes


async def on_startup(app):
    logging.info('sse.on_startup')
    app['streams'] = weakref.WeakSet()


async def on_shutdown(app):
    logging.info('sse.on_shutdown')
    for stream in set(app['streams']):
        await stream.write_eof()


async def make_app(**options):
    app = web.Application()
    app.add_routes(getRoutes())

    app.on_startup.append(on_startup)
    app.on_shutdown.append(on_shutdown)

    return app
