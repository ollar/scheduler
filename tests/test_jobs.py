import asyncio
from json import dumps
from datetime import datetime, timedelta
from .fixtures import client
from .utils import get_auth_token, get_now, get_utc_next_run_time


async def test_get_jobs_unauthorized(client):
    resp = await client.get('/jobs')
    assert resp.status == 403


async def test_get_jobs_list(client):
    auth = get_auth_token(client)
    resp = await client.get('/jobs', headers={
        "Authorization": f"Bearer {auth}"
    })
    assert resp.status == 200
    json = await resp.json()
    assert json == []


async def test_post_job(client):
    auth = get_auth_token(client)
    now = get_now()
    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger': 'interval',
        'trigger_args': {
            'seconds': 1,
            'start_date': str(now),
            'end_date': str(now + timedelta(seconds=5))
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })
    assert resp.status == 201
    post_json = await resp.json()

    assert post_json['ent_id'] is not None

    resp = await client.get(f"/jobs/{post_json['_id']}", headers={
        "Authorization": f"Bearer {auth}"
    })
    assert resp.status == 200
    get_json = await resp.json()


async def test_update_job(client):
    auth = get_auth_token(client)
    now = get_now()
    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger': 'interval',
        'trigger_args': {
            'seconds': 1,
            'start_date': str(now),
            'end_date': str(now + timedelta(seconds=5))
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })
    assert resp.status == 201
    post_json = await resp.json()

    assert post_json['ent_id'] is not None

    resp = await client.put(f"/jobs/{post_json['_id']}", data=dumps({
        'name': 'job triggered',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hey"',
        'trigger': 'interval',
        'trigger_args': {
            'seconds': 5,
            'start_date': str(now),
            'end_date': str(now + timedelta(seconds=5))
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 200

    resp = await client.get(f"/jobs/{post_json['_id']}", headers={
        "Authorization": f"Bearer {auth}"
    })
    assert resp.status == 200
    get_json = await resp.json()


async def test_delete_job(client):
    auth = get_auth_token(client)
    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger': 'interval',
        'trigger_args': {
            "days": 1
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })
    assert resp.status == 201
    post_json = await resp.json()

    resp = await client.delete(f"/jobs/{post_json['_id']}", headers={
        "Authorization": f"Bearer {auth}"
    })
    assert resp.status == 200

    resp = await client.get(f"/jobs/{post_json['_id']}", headers={
        "Authorization": f"Bearer {auth}"
    })
    resp_json = await resp.json()

    assert resp_json['deleted'] == True
