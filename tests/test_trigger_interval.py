import asyncio
from json import dumps
from datetime import datetime, timedelta
from .fixtures import client
from .utils import get_auth_token, get_now, get_utc_next_run_time


"""
Parameters
        weeks (int) – number of weeks to wait
        days (int) – number of days to wait
        hours (int) – number of hours to wait
        minutes (int) – number of minutes to wait
        seconds (int) – number of seconds to wait
        start_date (datetime|str) – starting point for the interval calculation
        end_date (datetime|str) – latest possible date/time to trigger on
        timezone (datetime.tzinfo|str) – time zone to use for the date/time calculations
        jitter (int|None) – delay the job execution by jitter seconds at most
"""


async def test_trigger_interval(client):
    """
    Schedule job_function to be called every two days
    """
    auth = get_auth_token(client)

    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered by interval',
        'trigger': 'interval',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger_args': {
            'days': 2
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 201

    resp_json = await resp.json()
    assert resp_json['ent_id'] is not None
    assert resp_json['next_run_time'] is not None


async def test_trigger_date_interval_with_multi_triggers(client):
    """
    scheduler takes minimal interval
    """
    auth = get_auth_token(client)
    now = get_now()
    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered by interval',
        'trigger': 'interval',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger_args': {
            'days': 2,
            'hours': 2,
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 201

    resp_json = await resp.json()
    assert resp_json['ent_id'] is not None
    assert resp_json['next_run_time'] is not None

    dt = get_utc_next_run_time(resp_json)

    delta = dt - now
    delta_secs = delta.seconds

    assert delta_secs == 2 * 60 * 60


async def test_trigger_interval_with_no_trigger_args(client):
    """
    Schedule job_function to be called every two days
    """
    auth = get_auth_token(client)

    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered by interval',
        'trigger': 'interval',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger_args': {
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 400


async def test_trigger_interval_with_invalid_days(client):
    """
    Schedule job_function to be called every two days
    """
    auth = get_auth_token(client)

    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered by interval',
        'trigger': 'interval',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger_args': {
            'days': -2
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 400


async def test_post_interval_job(client):
    auth = get_auth_token(client)
    now = get_now()
    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger': 'interval',
        'trigger_args': {
            'seconds': 1,
            'start_date': str(now),
            'end_date': str(now + timedelta(seconds=5))
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })
    assert resp.status == 201
    post_json = await resp.json()

    assert post_json['ent_id'] is not None
    assert post_json['next_run_time'] is not None

    await asyncio.sleep(2)

    resp = await client.get(f"/jobs/{post_json['_id']}", headers={
        "Authorization": f"Bearer {auth}"
    })
    assert resp.status == 200
    get_json = await resp.json()

    assert int(get_json['next_run_time']) - int(post_json['next_run_time']) == 2000
