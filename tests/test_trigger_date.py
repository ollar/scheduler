import asyncio
from json import dumps
from datetime import datetime, timedelta
from .fixtures import client
from .utils import get_auth_token, get_now, get_utc_next_run_time


"""
Parameters
        run_date (datetime|str) – the date/time to run the job at
        timezone (datetime.tzinfo|str) – time zone for run_date if it doesn’t have one already
"""

async def test_trigger_date(client):
    auth = get_auth_token(client)
    now = get_now()
    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered by date',
        'trigger': 'date',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger_args': {
            'run_date': str(now + timedelta(seconds=5))
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 201

    resp_json = await resp.json()
    assert resp_json['ent_id'] is not None
    assert resp_json['next_run_time'] is not None

    dt = get_utc_next_run_time(resp_json)

    delta = dt - now
    delta_secs = delta.seconds

    assert delta_secs == 4

    resp = await client.get(f'/jobs/{resp_json["_id"]}', headers={
        "Authorization": f"Bearer {auth}"
    })

    resp_json = await resp.json()

    dt = get_utc_next_run_time(resp_json)

    delta = dt - now
    delta_secs = delta.seconds

    assert delta_secs == 4


async def test_trigger_date_immediately(client):
    auth = get_auth_token(client)
    now = get_now()
    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered by date',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger': 'date',
        'trigger_args': {
            'run_date': str(now + timedelta(seconds=1))
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    resp_json = await resp.json()
    assert resp_json['ent_id'] is not None
    assert resp_json['next_run_time'] is not None

    dt = get_utc_next_run_time(resp_json)

    delta = dt - now
    delta_secs = delta.seconds

    assert delta_secs == 0


async def test_trigger_date_is_empty(client):
    auth = get_auth_token(client)
    now = get_now()
    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered by date',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger': 'date',
        'trigger_args': {

        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 400


async def test_trigger_date_in_past(client):
    auth = get_auth_token(client)
    now = get_now()
    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered by date',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger': 'date',
        'trigger_args': {
            'run_date': str(now - timedelta(seconds=10))
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 400


async def test_post_date_job(client):
    auth = get_auth_token(client)
    now = get_now()
    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered by date',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger': 'date',
        'trigger_args': {
            'run_date': str(now + timedelta(seconds=3))
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })
    assert resp.status == 201
    post_json = await resp.json()


    assert post_json['ent_id'] is not None
    assert post_json['next_run_time'] is not None

    dt = get_utc_next_run_time(post_json)

    delta = dt - now
    delta_secs = delta.seconds

    assert delta_secs == 2
