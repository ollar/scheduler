import pytest
from app.main import make_app


@pytest.fixture
async def client(aiohttp_client):
    return await aiohttp_client(await make_app(mongo_drop_table_on_init=True, mongo_db_name="test", secret="test"))