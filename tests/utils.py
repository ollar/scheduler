import jwt
from datetime import datetime, timedelta
from pytz import utc

user = {
    'user_id': 1
}


def get_auth_token(client):
    key = client.app['configs'].secret
    return jwt.encode(user, key, algorithm="HS256")


def get_now():
    return datetime.now(tz=utc)


def get_utc_next_run_time(json):
    ts = int(json['next_run_time']) / 1000
    return datetime.fromtimestamp(ts).astimezone(utc)