import asyncio
from json import dumps
from datetime import datetime, timedelta
from .fixtures import client
from .utils import get_auth_token, get_now, get_utc_next_run_time


"""
Parameters
        year (int|str) – 4-digit year
        month (int|str) – month (1-12)
        day (int|str) – day of month (1-31)
        week (int|str) – ISO week (1-53)
        day_of_week (int|str) – number or name of weekday (0-6 or mon,tue,wed,thu,fri,sat,sun)
        hour (int|str) – hour (0-23)
        minute (int|str) – minute (0-59)
        second (int|str) – second (0-59)
        start_date (datetime|str) – earliest possible date/time to trigger on (inclusive)
        end_date (datetime|str) – latest possible date/time to trigger on (inclusive)
        timezone (datetime.tzinfo|str) – time zone to use for the date/time calculations (defaults to scheduler timezone)
        jitter (int|None) – delay the job execution by jitter seconds at most
"""

async def test_post_cron_job(client):
    auth = get_auth_token(client)
    now = get_now()
    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger': 'cron',
        'trigger_args': {
            'minute': 1,
            'start_date': str(now),
            'end_date': str(now + timedelta(minutes=60))
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })
    assert resp.status == 201
    post_json = await resp.json()

    assert post_json['ent_id'] is not None
    assert post_json['next_run_time'] is not None


async def test_trigger_cron(client):
    auth = get_auth_token(client)
    now = get_now()
    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered by cron',
        'trigger': 'cron',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger_args': {
            "day": 1
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 201


async def test_trigger_cron_2(client):
    auth = get_auth_token(client)
    now = get_now()
    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered by cron',
        'trigger': 'cron',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger_args': {
            "day": 1,
            "day_of_week": 1
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 201


async def test_trigger_cron_3(client):
    auth = get_auth_token(client)
    now = get_now()
    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered by cron',
        'trigger': 'cron',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger_args': {
            "month": "*",
            "day_of_week": 1,
            "hour": "*",
            "minute": 30,
            "start_date": str(now + timedelta(hours=2)),
            "end_date": str(now + timedelta(days=30))
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 201


async def test_trigger_cron_no_trigger_args(client):
    auth = get_auth_token(client)
    now = get_now()
    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered by cron',
        'trigger': 'cron',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger_args': {
            "start_date": str(now + timedelta(hours=2)),
            "end_date": str(now + timedelta(days=30))
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 400


async def test_trigger_cron_invalid(client):
    auth = get_auth_token(client)
    now = get_now()
    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered by cron',
        'trigger': 'cron',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger_args': {
            "year": "*",
            "month": "*",
            "hour": "bueeee",
            "start_date": str(now + timedelta(hours=2)),
            "end_date": str(now + timedelta(days=30))
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 400


async def test_trigger_cron_invalid_2(client):
    auth = get_auth_token(client)
    now = get_now()
    resp = await client.post('/jobs', data=dumps({
        'name': 'job triggered by cron',
        'trigger': 'cron',
        'image_name': 'ubuntu:latest',
        'script': 'echo "hi"',
        'trigger_args': {
            "year": "*",
            "month": "*",
            "hour": 25,
            "start_date": str(now + timedelta(hours=2)),
            "end_date": str(now + timedelta(days=30))
        }
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 400
