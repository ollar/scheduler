from bson import ObjectId
from .base import Adapter, DatabaseRequired, RequestNotAcknowledged
from pymongo.results import InsertOneResult, InsertManyResult


class AppAdapter(Adapter):
    def __init__(self, db, configs):
        if not db: raise DatabaseRequired
        self.db = db
        self.configs = configs


    def find_record(self, collection_name, entity_id, **kwargs):
        return self.db[collection_name].find_one({"_id": ObjectId(entity_id)})


    def query_record(self, collection_name, find:dict={}, **kwargs):
        return self.db[collection_name]\
                    .find_one(find)


    def query(self, collection_name, find:dict={}, **kwargs):
        result = self.db[collection_name]\
                    .find(find)
        return [_doc for _doc in result]


    def create_record(self, collection_name, data: object, **kwargs):
        request = kwargs.get('request')
        auth_data = request['auth_data']

        if auth_data and auth_data.get('user_id'):
            data['_uid'] = auth_data['user_id']

        _mongo_res = self.db[collection_name].insert_one(data)

        if not _mongo_res.acknowledged:
            raise RequestNotAcknowledged

        return self.find_record(collection_name, _mongo_res.inserted_id, request=request)


    def update_record(self, collection_name, entity_id, data, **kwargs):
        request = kwargs.get('request')
        _current_record = self.find_record(collection_name, entity_id, request=request)

        if _current_record:
            data['_uid'] = _current_record.get('_uid')

        _mongo_res = self.db[collection_name].replace_one({"_id": ObjectId(entity_id)}, data)

        if not _mongo_res.acknowledged:
            raise RequestNotAcknowledged

        return self.find_record(collection_name, entity_id, request=request)


    def patch_record(self, collection_name, entity_id, data, **kwargs):
        request = kwargs.get('request')

        _mongo_res = self.db[collection_name].update_one(
            {"_id": ObjectId(entity_id)},
            {"$set": data}
        )

        if not _mongo_res.acknowledged:
            raise RequestNotAcknowledged

        return self.find_record(collection_name, entity_id, request=request)


    def delete_record(self, collection_name, entity_id, **kwargs):
        _mongo_res = self.db[collection_name].delete_one({"_id": ObjectId(entity_id)})

        if not _mongo_res.acknowledged:
            raise RequestNotAcknowledged

        return { "result": _mongo_res.acknowledged }
