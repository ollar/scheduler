from aiohttp import web

from utils import json_response, _get_user_from_bearer, parse_request_path, collection_exists
from .errors import FORBIDDEN, NOT_FOUND

from aiohttp.web_exceptions import HTTPNotFound, HTTPForbidden, HTTPBadRequest

from apscheduler.jobstores.base import JobLookupError
from app.exceptions import NotFountError, AccessRestictedException, JwtException, ValidatorError
from jwt.exceptions import ExpiredSignatureError, InvalidSignatureError, DecodeError
from app.errors import TOKEN_EXPIRED, TOKEN_INVALID
from ott.tokens_store import read_token


@web.middleware
async def cors_middleware(request, handler):
    configs = request.app['configs']

    headers = {
        'Access-Control-Allow-Origin': configs.app_headers_allow_origin,
        'Access-Control-Allow-Credentials': configs.app_headers_allow_credentials,
        'Access-Control-Allow-Headers': configs.app_headers_allow_headers,
        'Access-Control-Allow-Methods': configs.app_headers_allow_methods,
    }

    if request.method == 'OPTIONS':
        response = web.Response()
    else:
        response = await handler(request)
    response.headers.update(headers)

    return response


@web.middleware
async def excetions_handler(request, handler):
    def error(e, status):
        return json_response(data={ "error": str(e) }, status=status)

    try:
        result = await handler(request)
    except JwtException as e:
        return error(e, status=HTTPForbidden.status_code)
    except AccessRestictedException as e:
        return error(FORBIDDEN, status=HTTPForbidden.status_code)
    except (NotFountError, JobLookupError) as e:
        return error(NOT_FOUND, status=HTTPNotFound.status_code)

    except ExpiredSignatureError:
        return error(TOKEN_EXPIRED, status=HTTPForbidden.status_code)
    except (InvalidSignatureError, DecodeError):
        return error(TOKEN_INVALID, status=HTTPForbidden.status_code)

    except ValidatorError as e:
        return error(e, status=HTTPBadRequest.status_code)

    return result


@web.middleware
async def patch_request_with_auth_data(request, handler):
    SECURITY_HEADER_KEY = 'Authorization'
    SECURITY_HEADER_KEY_PREFIX = 'Bearer '

    secret = request.app.get('configs').secret

    def get_access_token(request):
        return request.headers.get(SECURITY_HEADER_KEY, '')\
            .replace(SECURITY_HEADER_KEY_PREFIX, '')

    if (request.query.get('token')):
        auth_data = read_token(request.query.get('token'))
    else:
        bearer = get_access_token(request)
        auth_data = _get_user_from_bearer(bearer, secret)

    request['auth_data'] = auth_data

    return await handler(request)


@web.middleware
async def access_check_handler(request, handler):
    auth_data = request['auth_data']
    adapter = request.app['adapter']

    if not auth_data:
        raise AccessRestictedException

    _, entity_id, collection_list_name = parse_request_path(request)
    request_to_entity = collection_list_name and entity_id and collection_exists(request, collection_list_name)

    if request_to_entity:
        entity = adapter.find_record(f"{collection_list_name}__meta", entity_id)

        if not entity or not auth_data.get('user_id') == entity.get('_uid'):
            raise AccessRestictedException

    return await handler(request)