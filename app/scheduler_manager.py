from pytz import utc

from apscheduler.schedulers.asyncio import AsyncIOScheduler
from apscheduler.executors.asyncio import AsyncIOExecutor
from apscheduler.jobstores.mongodb import MongoDBJobStore


class SchedulerManager():
    """Manages scheduler functionality and act as adapter"""

    scheduler = None

    def __init__(self, mongo_client, db, configs):
        self.mongo_client = mongo_client
        self.configs = configs

    def create_scheduler(self):
        executors = {
            'default': AsyncIOExecutor(),
        }

        jobstores = {
            'default': MongoDBJobStore(client=self.mongo_client, database=self.configs.mongo_db_name)
        }

        job_defaults = {
            'max_instances': 2
        }

        scheduler = AsyncIOScheduler(executors=executors, jobstores=jobstores, timezone=utc, job_defaults=job_defaults)

        self.scheduler = scheduler

        return scheduler


    def add_job(self, func, trigger, trigger_args, *args, **kwargs):
        job = self.scheduler.add_job(func, trigger, *args, **kwargs, **trigger_args)

        return job


    def get_jobs(self):
        return self.scheduler.get_jobs()


    def get_job(self, job_id):
        return self.scheduler.get_job(job_id)


    def reschedule_job(self, job_id, trigger, **trigger_args):
        return self.scheduler.reschedule_job(job_id=job_id, trigger=trigger, **trigger_args)


    def remove_job(self, job_id):
        return self.scheduler.remove_job(job_id)


    def remove_all_jobs(self):
        return self.scheduler.remove_all_jobs()


    def shutdown(self):
        return self.scheduler.shutdown()
