import os

SECRET_FILE_PATH = os.path.join(os.getcwd(), 'secret')
ENV = os.getenv('ENV', 'prod')

settings = {}


with open(SECRET_FILE_PATH, 'rb') as f:
    secret = f.read()


class Configs():
    app_name = 'scheduler'
    mongo_user = ''
    mongo_pass = ''
    mongo_host = 'localhost'
    mongo_port = 27017
    mongo_db_name = 'scheduler'
    mongo_drop_table_on_init = False
    app_headers_allow_origin = '*'
    app_headers_allow_credentials = 'true'
    app_headers_allow_headers = '*'
    app_headers_allow_methods = '*'
    list_length = 50
    secret = secret
    ENV = ENV

    def __init__(self, **options):
        settings.update(options)

        for key, value in settings.items():
            setattr(self, key, value)
