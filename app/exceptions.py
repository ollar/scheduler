class NotFountError(Exception):
    """ Entity not fount """


class AccessRestictedException(Exception):
    """ Exception raised on access rules check """


class JwtException(Exception):
    """ Exception raised on jwt token decoding """


class ValidatorError(Exception):
    """ Exception raised on data validation errors """

    def __init__(self, errors={}):
        self.errors = errors

    def __str__(self):
        return "\n".join([f"{key}: {', '.join(value)}" for key, value in self.errors.items()])