from aiohttp import web
from bson import ObjectId
from utils import json_response, serialize_job
from utils.scheme_validator import validate_data_scheme

from .schemes import job_schema
from .exceptions import NotFountError
from .docker_manager import run_job

from sse.messages import messenger


routes = web.RouteTableDef()


@routes.get('/jobs')
async def get_all_jobs(request):
    auth_data = request['auth_data']
    adapter = request.app['adapter']

    jobs_meta = adapter.query('jobs__meta', {"_uid": auth_data['user_id']})

    return json_response(jobs_meta)


@routes.get('/jobs/{job_id}')
async def get_job(request):
    auth_data = request['auth_data']
    adapter = request.app['adapter']
    job_id = request.match_info['job_id']
    scheduler_manager = request.app['scheduler_manager']



    messenger.add_message(auth_data['user_id'], {
        'event': 'info',
        'data': 'checked job'
    })

    job_meta = adapter.find_record("jobs__meta", job_id)

    if not job_meta:
        raise NotFountError

    if job_meta.get('deleted'):
        return json_response(job_meta)

    job = scheduler_manager.get_job(job_meta['ent_id'])

    if not job:
        raise NotFountError

    return json_response(dict(**job_meta, **serialize_job(job)))


@routes.post('/jobs')
@validate_data_scheme(job_schema)
async def create_job(request):
    auth_data = request['auth_data']
    adapter = request.app['adapter']
    scheduler_manager = request.app['scheduler_manager']

    _id = ObjectId()

    json = await request.json()
    job = scheduler_manager.add_job(
        func=run_job,
        args=[auth_data['user_id'], str(_id)],
        kwargs={"image": "ubuntu"},
        trigger=json['trigger'],
        trigger_args=json['trigger_args'],
    )

    job_meta = adapter.create_record("jobs__meta", dict(
        ent_id=job.id,
        _id=_id,
        **json,
    ), request=request)

    return json_response(dict(**job_meta, **serialize_job(job)), status=201)


@routes.put('/jobs/{job_id}')
@validate_data_scheme(job_schema)
async def update_job(request):
    adapter = request.app['adapter']
    json = await request.json()
    job_id = request.match_info['job_id']
    scheduler_manager = request.app['scheduler_manager']

    job_meta = adapter.find_record("jobs__meta", job_id)

    if not job_meta:
        raise NotFountError

    job = scheduler_manager.reschedule_job(
        job_meta['ent_id'],
        trigger=json['trigger'],
        **json['trigger_args']
    )

    job_meta = adapter.patch_record("jobs__meta", job_id, dict(
        ent_id=job.id,
        **json,
    ), request=request)

    return json_response(dict(**job_meta, **serialize_job(job)))


@routes.delete('/jobs/{job_id}')
async def delete_job(request):
    adapter = request.app['adapter']
    scheduler_manager = request.app['scheduler_manager']
    job_id = request.match_info['job_id']

    job_meta = adapter.find_record("jobs__meta", job_id)

    if not job_meta:
        raise NotFountError

    scheduler_manager.remove_job(job_meta['ent_id'])

    job_meta = adapter.patch_record("jobs__meta", job_id, dict(
        deleted=True
    ), request=request)

    return json_response(dict(**job_meta))


def get_routes():
    return routes