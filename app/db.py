import os

ENV = os.getenv('ENV', 'prod')

if ENV == 'test':
    from mongomock import MongoClient
else:
    from pymongo import MongoClient


async def initDB(configs):
    mongo_client = MongoClient(host=configs.mongo_host, port=configs.mongo_port)

    if configs.mongo_drop_table_on_init:
        mongo_client.drop_database(configs.mongo_db_name)

    db = mongo_client[configs.mongo_db_name]
    return (mongo_client, db)
