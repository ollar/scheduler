from pytz import utc
from datetime import datetime


job_schema = {
    'name': {
        'type': 'string',
        'required': True,
    },
    'image_name': {
        'type': 'string',
        'required': True,
    },
    'script': {
        'type': 'string',
        'required': False,
        'nullable': True,
    },
    'trigger': {
        'type': 'string',
        'required': True,
        'oneof': [
            {'regex': '^interval$'},
            {'regex': '^cron$'},
            {'regex': '^date$'}
        ]
    },
    'trigger_args': {
        'type': 'dict',
        'required': True,
        'allow_unknown': True
    }
}


def date_check(field, value, error):
    now = datetime.now(tz=utc)
    try:
        date = datetime.fromisoformat(value).astimezone(utc)
    except Exception as err:
        error(field, str(err))
        return

    if date and date < now:
        error(field, "Must be date in future")


date_trigger_args_schema = {
    "run_date": {
        "type": "string",
        'required': True,
        "check_with": date_check
    }
}


interval_trigger_args_schema = {
    "weeks": {
        "type": "integer",
        'nullable': True,
        "min": 1,
    },
    "days": {
        "type": "integer",
        'nullable': True,
        "min": 1,
    },
    "hours": {
        "type": "integer",
        'nullable': True,
        "min": 1,
    },
    "minutes": {
        "type": "integer",
        'nullable': True,
        "min": 1,
    },
    "seconds": {
        "type": "integer",
        'nullable': True,
        "min": 1,
    },
    'start_date': {
        'type': 'string',
        'nullable': True
    },
    'end_date': {
        'type': 'string',
        'nullable': True
    },
    "jitter": {
        "type": "integer",
        'required': False,
        'nullable': True,
    }
}

cron_trigger_args_schema = {
    "month": {
        "type": ["integer", "string"],
        "nullable": True,
    },
    "day": {
        "type": ["integer", "string"],
        "nullable": True,
    },
    "day_of_week": {
        "type": ["integer", "string"],
        "nullable": True,
    },
    "hour": {
        "type": ["integer", "string"],
        "nullable": True,
    },
    "minute": {
        "type": ["integer", "string"],
        "nullable": True,
    },
    'start_date': {
        'type': 'string',
        'nullable': True
    },
    'end_date': {
        'type': 'string',
        'nullable': True
    },
    "jitter": {
        "type": "integer",
        'required': False,
        'nullable': True,
    }
}