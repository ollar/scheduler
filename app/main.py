from aiohttp import web
import asyncio
import logging
import uvloop

from .scheduler_manager import SchedulerManager
from .docker_manager import teardown_docker_instances
from .routes import get_routes
from .db import initDB
from .configs import Configs
from .middlewares import excetions_handler, access_check_handler, patch_request_with_auth_data, cors_middleware
from adapters.app import AppAdapter

from sse.main import make_app as make_sse_app
from ott.main import make_app as make_ott_app


logging.basicConfig(level=logging.INFO)
asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())


async def on_startup(app):
    logging.debug('App is starting up')

    (mongo_client, db) = await initDB(app['configs'])
    app['mongo_client'] = mongo_client
    app['db'] = db

    scheduler_manager = SchedulerManager(
        mongo_client=mongo_client,
        db=db,
        configs=app['configs']
    )
    app['scheduler_manager'] = scheduler_manager
    scheduler = scheduler_manager.create_scheduler()
    scheduler.start()

    # docker_manager = DockerManager(configs=app['configs'])
    # app['docker_manager'] = docker_manager

    adapter = AppAdapter(db=db, configs=app['configs'])
    app['adapter'] = adapter


async def on_shutdown(app):
    logging.debug('App is cleaning up')
    scheduler_manager = app['scheduler_manager']
    scheduler_manager.shutdown()

    # docker_manager = app['docker_manager']
    # docker_manager.teardown()

    teardown_docker_instances()


async def make_app(**options):
    configs = Configs(**options)
    app = web.Application(
        middlewares=[
            cors_middleware,
            excetions_handler,
            patch_request_with_auth_data,
            access_check_handler,
        ]
    )

    ott_app = await make_ott_app()
    sse_app = await make_sse_app()

    app['configs'] = configs
    app.add_routes(get_routes())
    app.add_subapp('/sse/', sse_app)
    app.add_subapp('/ott/', ott_app)

    app.on_startup.append(on_startup)
    app.on_shutdown.append(on_shutdown)

    return app


if __name__ == "__main__":
    web.run_app(make_app())