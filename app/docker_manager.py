import docker
import logging
from functools import wraps
import dramatiq
import os
from dramatiq.brokers.redis import RedisBroker
from dramatiq.brokers.stub import StubBroker
from dramatiq.middleware import CurrentMessage
from sse.messages import messenger
from weakref import WeakSet
from json import dumps


if os.getenv("ENV") == "test":
    broker = StubBroker()
    broker.emit_after("process_boot")
else:
    broker = RedisBroker()


broker.add_middleware(CurrentMessage())
dramatiq.set_broker(broker)


clients = WeakSet([])


def run_job(*args, **kwargs):
    return _run_job_in_docker.send(*args, **kwargs)


# TODO: add error handling there

@dramatiq.actor(max_retries=3, time_limit=600000)
def _run_job_in_docker(user_id, job_id, image="ubuntu", cmd="echo 'hi'"):
    global clients

    messenger.add_message(user_id, {
        'event': 'job_started',
        'data': dumps({
            "job_id": job_id
        })
    })

    client = _create_client()
    container = client.containers.create(image, tty=True)
    container.start()

    logging.info(CurrentMessage.get_current_message())

    (exit_code, output) = container.exec_run(cmd, stream=True)

    for line in output:
        messenger.add_message(user_id, {
            'event': 'job_progress',
            'data': dumps({
                "job_id": job_id,
                "log": line.strip().decode('utf-8')
            })
        })
        logging.info(line.strip())

    logging.info(f'Job complete: {exit_code}')

    messenger.add_message(user_id, {
        'event': 'job_completed',
        'data': dumps({
            "job_id": job_id
        })
    })

    logging.info(messenger.get_messages(user_id))

    container.stop(timeout=5)
    container.remove()

    client.close()
    clients.remove(client)


def _create_client(base_url='unix://var/run/docker.sock', *args, **kwargs):
    global clients

    client = docker.DockerClient(base_url=base_url, *args, **kwargs)

    clients.add(client)

    logging.debug(client.version())

    return client


def teardown_docker_instances():
    global clients

    for client in clients:
        logging.debug(f'removing stopped containers: {client.containers.prune()}')
        for container in client.containers.list():
            container.stop()
        client.close()
        clients.discard(client)
