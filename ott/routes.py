from aiohttp import web
from utils import json_response
from .tokens_store import add_token


routes = web.RouteTableDef()


@routes.get('/token')
async def websocket_handler(request):
    key = add_token(request['auth_data'])
    return json_response({"token": key})


def getRoutes():
    return routes
