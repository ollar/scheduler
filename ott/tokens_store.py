from nanoid import generate


tokens = {}


def add_token(auth_data):
    global tokens

    key = generate('1234567890abcdef', 10)

    tokens[key] = auth_data
    return key


def read_token(key):
    global tokens

    auth_data = tokens.get(key)

    if tokens.get(key):
        del tokens[key]

    return auth_data
