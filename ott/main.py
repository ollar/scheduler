from aiohttp import web
from .routes import getRoutes

async def make_app(**options):
    app = web.Application()
    app.add_routes(getRoutes())

    # app.on_startup.append(on_startup)
    # app.on_cleanup.append(on_cleanup)

    return app
