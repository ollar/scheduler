SHELL := /bin/bash

stop_mongo:
	-docker stop mongoproxy || true && docker rm mongoproxy || true

start_mongo: stop_mongo
	docker run -d -p 27017:27017 -v ~/mongo_data:/data/db --name mongoproxy mongo

check_mongo_running:
	docker inspect -f '{{.State.Running}}' mongoproxy

stop_ampq:
	-docker stop rabbitproxy || true && docker rm rabbitproxy || true

start_ampq: stop_ampq
	docker run -d -p 5672:5672 --hostname rabbitmqhost -v "rabbitmq_log:/var/log/rabbitmq" -v "rabbitmq_data:/var/lib/rabbitmq" --name rabbitproxy rabbitmq:3

stop_redis:
	-docker stop redisproxy || true && docker rm redisproxy || true

start_redis: stop_redis
	docker run -d -p 6379:6379 --hostname rabbitmqhost -v ~/redis_data:/data --name redisproxy redis

start_dramatiq:
	dramatiq app.docker_manager

docker_stop_all:
	docker stop $(docker ps -q)

dev:
	python run_dev.py

dev_no_db:
	ENV='test' python run_dev.py

prod:
	gunicorn -D 'app.main:make_app' --workers=3 --bind 0.0.0.0:8080 --worker-class aiohttp.worker.GunicornWebWorker --access-logfile app_access.log --error-logfile app_error.log

tests:
	ENV='test' pytest -rs -v

ci-test: start_mongo
	make test


.PHONY: dev tests prod
