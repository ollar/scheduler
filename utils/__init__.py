from aiohttp import web
import json
import jwt
import pymongo
from json.decoder import JSONDecodeError
from bson import ObjectId
from functools import partial
from datetime import datetime

get_path_array = lambda path: path.split('/')


def default_json(o):
    if isinstance(o, ObjectId):
        return str(o)
    if isinstance(o, datetime):
        return o.timestamp() * 1000
    return json.JSONEncoder.default(o)  # pylint: disable=no-value-for-parameter


json_dumps = partial(json.dumps, default=default_json)
json_response = partial(web.json_response, dumps=json_dumps)


def parse_request_path(request):
    path = get_path_array(request.path)
    path_array = path[1:]

    entity_id = path_array.pop()
    collection_list_name = ('/').join(path_array)

    return (path, entity_id, collection_list_name)


def validate_object_id(oid):
    return ObjectId.is_valid(oid)


def _get_user_from_bearer(bearer, secret):
    if not bearer:
        return None

    user = jwt.decode(bearer, secret, algorithms="HS256")

    if user:
        return user

    return None


def serialize_job(job):
    return dict(
        next_run_time=job.next_run_time,
        pending=job.pending,
    )


def collection_exists(request, collection_list_name):
    return collection_list_name in request.app['db'].list_collection_names()
