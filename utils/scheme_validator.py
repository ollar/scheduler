from functools import wraps
from cerberus import Validator

from app.schemes import date_trigger_args_schema, interval_trigger_args_schema, cron_trigger_args_schema
from app.exceptions import ValidatorError

from apscheduler.triggers.cron import CronTrigger
# from apscheduler.triggers.date import DateTrigger


def check_data_has_any_field(data: list, check: list) -> bool:
    return len(list(set(data) & set(check))) > 0


def validate_data_scheme(schema):
    def decorator(handler):
        @wraps(handler)
        async def wrapper(*args, **kwargs):
            request = args[0]
            json = await request.json()

            v = Validator()

            if not v.validate(json, schema):
                raise ValidatorError(errors=v.errors)

            if json['trigger'] == 'date':
                if not v.validate(json['trigger_args'], date_trigger_args_schema):
                    raise ValidatorError(errors=v.errors)
                # try:
                #     DateTrigger(**json['trigger_args'])
                # except Exception as err:
                #     raise ValidatorError(errors={"incorrect field": [str(err)]})

            if json['trigger'] == 'interval':
                if not v.validate(json['trigger_args'], interval_trigger_args_schema):
                    raise ValidatorError(errors=v.errors)

                if not check_data_has_any_field(json['trigger_args'].keys(), ['weeks', 'days', 'hours', 'minutes', 'seconds']):
                    raise ValidatorError(errors={"incorrect field": ["no args provided"]})

            if json['trigger'] == 'cron':
                if not v.validate(json['trigger_args'], cron_trigger_args_schema):
                    raise ValidatorError(errors=v.errors)

                if not check_data_has_any_field(json['trigger_args'].keys(), ['month', 'day', 'day_of_week', 'hour', 'minute']):
                    raise ValidatorError(errors={"incorrect field": ["no args provided"]})

                try:
                    CronTrigger(**json['trigger_args'])
                except Exception as err:
                    raise ValidatorError(errors={"incorrect field": [str(err)]})

            return await handler(*args, **kwargs)

        return wrapper
    return decorator